<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/attribute/library/ConstAttribute.php');
include($strRootPath . '/src/attribute/specification/controller/command/AttrSpecController.php');
include($strRootPath . '/src/attribute/boot/AttributeBootstrap.php');

include($strRootPath . '/src/type/library/ConstType.php');

include($strRootPath . '/src/type/type_string/library/ConstStringDataType.php');
include($strRootPath . '/src/type/type_string/model/StringDataType.php');

include($strRootPath . '/src/type/type_string/multi/library/ConstMultiStringDataType.php');
include($strRootPath . '/src/type/type_string/multi/model/MultiStringDataType.php');

include($strRootPath . '/src/type/type_text/library/ConstTextDataType.php');
include($strRootPath . '/src/type/type_text/model/TextDataType.php');

include($strRootPath . '/src/type/type_numeric/library/ConstNumericDataType.php');
include($strRootPath . '/src/type/type_numeric/model/NumericDataType.php');

include($strRootPath . '/src/type/type_numeric/positive/library/ConstPositiveNumericDataType.php');
include($strRootPath . '/src/type/type_numeric/positive/model/PositiveNumericDataType.php');

include($strRootPath . '/src/type/type_integer/library/ConstIntegerDataType.php');
include($strRootPath . '/src/type/type_integer/model/IntegerDataType.php');

include($strRootPath . '/src/type/type_integer/positive/library/ConstPositiveIntegerDataType.php');
include($strRootPath . '/src/type/type_integer/positive/model/PositiveIntegerDataType.php');

include($strRootPath . '/src/type/type_boolean/library/ConstBooleanDataType.php');
include($strRootPath . '/src/type/type_boolean/model/BooleanDataType.php');

include($strRootPath . '/src/type/type_date/library/ConstDateDataType.php');
include($strRootPath . '/src/type/type_date/model/DateDataType.php');

include($strRootPath . '/src/type/boot/TypeBootstrap.php');

include($strRootPath . '/src/cache/library/ConstCache.php');