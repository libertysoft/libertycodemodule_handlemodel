<?php
/**
 * Description :
 * This class allows to define attribute module bootstrap class.
 * Attribute module bootstrap allows to boot attribute module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\attribute\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;
use liberty_code\handle_model\attribute\specification\type\build\api\DataTypeBuilderInterface;
use liberty_code_module\handle_model\attribute\library\ConstAttribute;



class AttributeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Data type collection instance.
     * @var DataTypeCollectionInterface
     */
    protected $objDataTypeCollection;



    /**
     * DI: Data type builder instance.
     * @var DataTypeBuilderInterface
     */
    protected $objDataTypeBuilder;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DataTypeCollectionInterface $objDataTypeCollection
     * @param DataTypeBuilderInterface $objDataTypeBuilder
     */
    public function __construct(
        AppInterface $objApp,
        DataTypeCollectionInterface $objDataTypeCollection,
        DataTypeBuilderInterface $objDataTypeBuilder
    )
    {
        // Init properties
        $this->objDataTypeCollection = $objDataTypeCollection;
        $this->objDataTypeBuilder = $objDataTypeBuilder;

        // Call parent constructor
        parent::__construct($objApp, ConstAttribute::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Hydrate data type collection
        $this->objDataTypeBuilder->hydrateDataTypeCollection(
            $this->objDataTypeCollection,
            false
        );
    }



}