<?php
/**
 * Description :
 * This class allows to define attribute specification controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\attribute\specification\controller\command;

use liberty_code\controller\controller\model\DefaultController;

use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;



class AttrSpecController extends DefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Attribute specification instance.
     * @var AttrSpecInterface
     */
    protected $objAttrSpec;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttrSpecInterface $objAttrSpec
     */
    public function __construct(
        AttrSpecInterface $objAttrSpec
    )
    {
        // Init properties
        $this->objAttrSpec = $objAttrSpec;

        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        // Return result
        return true;
    }





    // Methods action
    // ******************************************************************************

    /**
     * Action to get attribute data types.
     *
     * @return DefaultResponse
     */
    public function actionGetDataType()
    {
        // Init var
        $tabDataType = $this->objAttrSpec->getTabDataType();

        // Get render
        $strRender = '';
        foreach($tabDataType as $strDataType)
        {
            $strRender .=
                ((trim($strRender) != '') ? PHP_EOL : '') .
                $strDataType;
        }

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



}