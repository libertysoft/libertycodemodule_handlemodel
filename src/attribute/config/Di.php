<?php

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;
use liberty_code\handle_model\attribute\specification\type\model\DefaultDataTypeCollection;
use liberty_code\handle_model\attribute\specification\type\factory\api\DataTypeFactoryInterface;
use liberty_code\handle_model\attribute\specification\type\factory\standard\model\StandardDataTypeFactory;
use liberty_code\handle_model\attribute\specification\type\build\api\DataTypeBuilderInterface;
use liberty_code\handle_model\attribute\specification\type\build\model\DefaultDataTypeBuilder;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\handle_model\attribute\specification\standard\model\StandardAttrSpec;



return array(
    // Attribute specification data type services
    // ******************************************************************************

    'attribute_specification_data_type_collection' => [
        'source' => DefaultDataTypeCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_factory' => [
        'source' => StandardDataTypeFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => DateTimeFactoryInterface::class],
            ['type' => 'mixed', 'value' => null],
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_builder' => [
        'source' => DefaultDataTypeBuilder::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'attribute_specification_data_type_factory'],
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/data_source']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Attribute specification services
    // ******************************************************************************

    'attribute_specification' => [
        'source' => StandardAttrSpec::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'attribute_specification_data_type_collection'],
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/config'],
            ['type' => 'dependency', 'value' => 'attribute_specification_cache_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    DataTypeCollectionInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'attribute_specification_data_type_collection'],
        'option' => [
            'shared' => true,
        ]
    ],

    DataTypeFactoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'attribute_specification_data_type_factory'],
        'option' => [
            'shared' => true,
        ]
    ],

    DataTypeBuilderInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'attribute_specification_data_type_builder'],
        'option' => [
            'shared' => true,
        ]
    ],

    AttrSpecInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'attribute_specification'],
        'option' => [
            'shared' => true,
        ]
    ]
);