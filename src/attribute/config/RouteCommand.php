<?php

use liberty_code_module\handle_model\attribute\specification\controller\command\AttrSpecController;



return array(
    // Attribute specification services
    // ******************************************************************************

    'attribute_specification_get_data_type' => [
        'source' => 'attribute:spec:get:data-type',
        'call' => [
            'class_path_pattern' => AttrSpecController::class . ':actionGetDataType'
        ],
        'description' => 'Get all attribute data types'
    ]
);