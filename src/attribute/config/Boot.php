<?php

use liberty_code_module\handle_model\attribute\boot\AttributeBootstrap;



return array(
    'attribute_bootstrap' => [
        'call' => [
            'class_path_pattern' => AttributeBootstrap::class . ':boot'
        ]
    ]
);