<?php

use liberty_code\handle_model\attribute\specification\type\build\model\DefaultDataTypeBuilder;
use liberty_code\handle_model\attribute\specification\standard\model\StandardAttrSpec;



return array(
    // Handle model configuration
    // ******************************************************************************

    'handle_model' => [
        'attribute' => [
            'specification' => [
                /**
                 * Attribute specification configuration format:
                 * @see StandardAttrSpec configuration format.
                 */
                'config' => [],

                'data_type' => [
                    /**
                     * Data type builder source data format (used to hydrate data type collection):
                     * @see DefaultDataTypeBuilder source data format.
                     */
                    'data_source' => []
                ]
            ]
        ]
    ]
);