<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_string\library;



class ConstStringDataType
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE = 'string';



}