<?php
/**
 * This class allows to define string data type class.
 * String data type is string data type,
 * which allows to manage entity attribute simple string value.
 *
 * String data type uses the following specified configuration:
 * [
 *     String data type configuration,
 *
 *     type(defined): "string",
 *
 *     multiline_require(defined): false,
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_string\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_string\model\StringDataType as BaseStringDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\library\ConstStringDataType as BaseConstStringDataType;
use liberty_code_module\handle_model\type\type_string\library\ConstStringDataType;



class StringDataType extends BaseStringDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstStringDataType::CONFIG_TYPE,
            BaseConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE => false,
            BaseConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            BaseConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false
        );
    }



}