<?php
/**
 * This class allows to define multi string data type class.
 * Multi string data type is string data type,
 * which allows to manage entity attribute index array value of simple string values.
 *
 * Multi string data type uses the following specified configuration:
 * [
 *     String data type configuration,
 *
 *     type(defined): "string[]",
 *
 *     multiple_require(defined): true,
 *
 *     multiple_unique_require(defined): true
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_string\multi\model;

use liberty_code_module\handle_model\type\type_string\model\StringDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\library\ConstStringDataType as BaseConstStringDataType;
use liberty_code_module\handle_model\type\type_string\multi\library\ConstMultiStringDataType;



class MultiStringDataType extends StringDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array_merge(
            parent::getTabFixConfig(),
            array(
                ConstDataType::TAB_CONFIG_KEY_TYPE => ConstMultiStringDataType::CONFIG_TYPE,
                BaseConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => true,
                BaseConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => true
            )
        );
    }



}