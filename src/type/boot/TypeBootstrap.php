<?php
/**
 * Description :
 * This class allows to define type module bootstrap class.
 * Type module bootstrap allows to boot type module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\handle_model\attribute\specification\type\api\DataTypeCollectionInterface;
use liberty_code_module\handle_model\type\library\ConstType;
use liberty_code_module\handle_model\type\type_string\model\StringDataType;
use liberty_code_module\handle_model\type\type_string\multi\model\MultiStringDataType;
use liberty_code_module\handle_model\type\type_text\model\TextDataType;
use liberty_code_module\handle_model\type\type_numeric\model\NumericDataType;
use liberty_code_module\handle_model\type\type_numeric\positive\model\PositiveNumericDataType;
use liberty_code_module\handle_model\type\type_integer\model\IntegerDataType;
use liberty_code_module\handle_model\type\type_integer\positive\model\PositiveIntegerDataType;
use liberty_code_module\handle_model\type\type_boolean\model\BooleanDataType;
use liberty_code_module\handle_model\type\type_date\model\DateDataType;



class TypeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Provider instance.
     * @var ProviderInterface
     */
    protected $objProvider;



    /**
     * DI: Data type collection instance.
     * @var DataTypeCollectionInterface
     */
    protected $objDataTypeCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider
     * @param DataTypeCollectionInterface $objDataTypeCollection
     */
    public function __construct(
        AppInterface $objApp,
        ProviderInterface $objProvider,
        DataTypeCollectionInterface $objDataTypeCollection
    )
    {
        // Init properties
        $this->objProvider = $objProvider;
        $this->objDataTypeCollection = $objDataTypeCollection;

        // Call parent constructor
        parent::__construct($objApp, ConstType::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Get data types
        $tabDataType = array_map(
            function($strClassPath)
            {
                return $this->objProvider->getFromClassPath($strClassPath);
            },
            array(
                StringDataType::class,
                MultiStringDataType::class,
                TextDataType::class,
                NumericDataType::class,
                PositiveNumericDataType::class,
                IntegerDataType::class,
                PositiveIntegerDataType::class,
                BooleanDataType::class,
                DateDataType::class
            )
        );

        // Hydrate data type collection
        $this->objDataTypeCollection->setTabDataType($tabDataType);
    }



}