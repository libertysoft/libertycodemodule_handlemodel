<?php

use liberty_code_module\handle_model\type\boot\TypeBootstrap;



return array(
    'attribute_type_bootstrap' => [
        'call' => [
            'class_path_pattern' => TypeBootstrap::class . ':boot'
        ]
    ]
);