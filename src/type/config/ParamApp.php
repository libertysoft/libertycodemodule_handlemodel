<?php

use liberty_code_module\handle_model\type\type_string\model\StringDataType;
use liberty_code_module\handle_model\type\type_string\multi\model\MultiStringDataType;
use liberty_code_module\handle_model\type\type_text\model\TextDataType;
use liberty_code_module\handle_model\type\type_numeric\model\NumericDataType;
use liberty_code_module\handle_model\type\type_numeric\positive\model\PositiveNumericDataType;
use liberty_code_module\handle_model\type\type_integer\model\IntegerDataType;
use liberty_code_module\handle_model\type\type_integer\positive\model\PositiveIntegerDataType;
use liberty_code_module\handle_model\type\type_boolean\model\BooleanDataType;
use liberty_code_module\handle_model\type\type_date\model\DateDataType;



return array(
    // Handle model configuration
    // ******************************************************************************

    'handle_model' => [
        'attribute' => [
            'specification' => [
                'data_type' => [
                    'string' => [
                        /**
                         * String data type configuration format:
                         * @see StringDataType configuration format.
                         */
                        'config' => []
                    ],

                    'multi_string' => [
                        /**
                         * Multi string data type configuration format:
                         * @see MultiStringDataType configuration format.
                         */
                        'config' => []
                    ],

                    'text' => [
                        /**
                         * Multi string data type configuration format:
                         * @see TextDataType configuration format.
                         */
                        'config' => []
                    ],

                    'numeric' => [
                        /**
                         * Numeric data type configuration format:
                         * @see NumericDataType configuration format.
                         */
                        'config' => []
                    ],

                    'positive_numeric' => [
                        /**
                         * Positive numeric data type configuration format:
                         * @see PositiveNumericDataType configuration format.
                         */
                        'config' => []
                    ],

                    'integer' => [
                        /**
                         * Integer data type configuration format:
                         * @see IntegerDataType configuration format.
                         */
                        'config' => []
                    ],

                    'positive_integer' => [
                        /**
                         * Positive integer data type configuration format:
                         * @see PositiveIntegerDataType configuration format.
                         */
                        'config' => []
                    ],

                    'boolean' => [
                        /**
                         * Boolean data type configuration format:
                         * @see BooleanDataType configuration format.
                         */
                        'config' => []
                    ],

                    'date' => [
                        /**
                         * Date data type configuration format:
                         * @see DateDataType configuration format.
                         */
                        'config' => []
                    ]
                ]
            ]
        ]
    ]
);