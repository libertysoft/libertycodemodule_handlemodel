<?php

use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code_module\handle_model\type\type_string\model\StringDataType;
use liberty_code_module\handle_model\type\type_string\multi\model\MultiStringDataType;
use liberty_code_module\handle_model\type\type_text\model\TextDataType;
use liberty_code_module\handle_model\type\type_numeric\model\NumericDataType;
use liberty_code_module\handle_model\type\type_numeric\positive\model\PositiveNumericDataType;
use liberty_code_module\handle_model\type\type_integer\model\IntegerDataType;
use liberty_code_module\handle_model\type\type_integer\positive\model\PositiveIntegerDataType;
use liberty_code_module\handle_model\type\type_boolean\model\BooleanDataType;
use liberty_code_module\handle_model\type\type_date\model\DateDataType;



return array(
    // Attribute specification data type services
    // ******************************************************************************

    'attribute_specification_data_type_string' => [
        'source' => StringDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/string/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_multi_string' => [
        'source' => MultiStringDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/multi_string/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_text' => [
        'source' => TextDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/text/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_numeric' => [
        'source' => NumericDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/numeric/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_positive_numeric' => [
        'source' => PositiveNumericDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/positive_numeric/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_integer' => [
        'source' => IntegerDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/integer/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_positive_integer' => [
        'source' => PositiveIntegerDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/positive_integer/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_boolean' => [
        'source' => BooleanDataType::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/boolean/config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_data_type_date' => [
        'source' => DateDataType::class,
        'argument' => [
            ['type' => 'class', 'value' => DateTimeFactoryInterface::class],
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/data_type/date/config']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);