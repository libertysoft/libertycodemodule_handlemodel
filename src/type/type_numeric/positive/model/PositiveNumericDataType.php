<?php
/**
 * This class allows to define positive numeric data type class.
 * Positive numeric data type is numeric data type,
 * which allows to manage entity attribute positive float numeric value.
 *
 * Positive numeric data type uses the following specified configuration:
 * [
 *     Numeric data type configuration,
 *
 *     type(defined): "positive_numeric",
 *
 *     greater_compare_value(defined): 0,
 *
 *     greater_equal_enable_require(defined): true
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_numeric\positive\model;

use liberty_code_module\handle_model\type\type_numeric\model\NumericDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\library\ConstNumericDataType;
use liberty_code_module\handle_model\type\type_numeric\positive\library\ConstPositiveNumericDataType;



class PositiveNumericDataType extends NumericDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array_merge(
            parent::getTabFixConfig(),
            array(
                ConstDataType::TAB_CONFIG_KEY_TYPE => ConstPositiveNumericDataType::CONFIG_TYPE,
                ConstNumericDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE => 0,
                ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE => true
            )
        );
    }



}