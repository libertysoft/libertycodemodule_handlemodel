<?php
/**
 * This class allows to define numeric data type class.
 * Numeric data type is numeric data type,
 * which allows to manage entity attribute float numeric value.
 *
 * Numeric data type uses the following specified configuration:
 * [
 *     Numeric data type configuration,
 *
 *     type(defined): "numeric",
 *
 *     integer_require(defined): false,
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_numeric\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model\NumericDataType as BaseNumericDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\library\ConstNumericDataType as BaseConstNumericDataType;
use liberty_code_module\handle_model\type\type_numeric\library\ConstNumericDataType;



class NumericDataType extends BaseNumericDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstNumericDataType::CONFIG_TYPE,
            BaseConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE => false,
            BaseConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            BaseConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false
        );
    }



}