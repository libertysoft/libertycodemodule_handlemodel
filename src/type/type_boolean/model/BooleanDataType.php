<?php
/**
 * This class allows to define boolean data type class.
 * Boolean data type is boolean data type,
 * which allows to manage entity attribute boolean value.
 *
 * Boolean data type uses the following specified configuration:
 * [
 *     Boolean data type configuration,
 *
 *     type(defined): "boolean",
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_boolean\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\model\BooleanDataType as BaseBooleanDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_boolean\library\ConstBooleanDataType as BaseConstBooleanDataType;
use liberty_code_module\handle_model\type\type_boolean\library\ConstBooleanDataType;



class BooleanDataType extends BaseBooleanDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstBooleanDataType::CONFIG_TYPE,
            BaseConstBooleanDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            BaseConstBooleanDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false
        );
    }



}