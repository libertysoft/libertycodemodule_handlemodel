<?php
/**
 * This class allows to define date data type class.
 * Date data type is date data type,
 * which allows to manage entity attribute date value.
 *
 * Date data type uses the following specified configuration:
 * [
 *     Date data type configuration,
 *
 *     type(defined): "date",
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_date\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_date\model\DateDataType as BaseDateDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\library\ConstDateDataType as BaseConstDateDataType;
use liberty_code_module\handle_model\type\type_date\library\ConstDateDataType;



class DateDataType extends BaseDateDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstDateDataType::CONFIG_TYPE,
            BaseConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            BaseConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false
        );
    }



}