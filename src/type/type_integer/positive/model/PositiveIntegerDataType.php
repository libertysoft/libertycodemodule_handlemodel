<?php
/**
 * This class allows to define positive integer data type class.
 * Positive integer data type is integer data type,
 * which allows to manage entity attribute positive integer numeric value.
 *
 * Positive integer data type uses the following specified configuration:
 * [
 *     Integer data type configuration,
 *
 *     type(defined): "positive_integer",
 *
 *     greater_compare_value(defined): 0,
 *
 *     greater_equal_enable_require(defined): true
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_integer\positive\model;

use liberty_code_module\handle_model\type\type_integer\model\IntegerDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\library\ConstNumericDataType;
use liberty_code_module\handle_model\type\type_integer\positive\library\ConstPositiveIntegerDataType;



class PositiveIntegerDataType extends IntegerDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array_merge(
            parent::getTabFixConfig(),
            array(
                ConstDataType::TAB_CONFIG_KEY_TYPE => ConstPositiveIntegerDataType::CONFIG_TYPE,
                ConstNumericDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE => 0,
                ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE => true
            )
        );
    }



}