<?php
/**
 * This class allows to define integer data type class.
 * Integer data type is numeric data type,
 * which allows to manage entity attribute integer numeric value.
 *
 * Integer data type uses the following specified configuration:
 * [
 *     Numeric data type configuration,
 *
 *     type(defined): "integer",
 *
 *     integer_require(defined): true,
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_integer\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model\NumericDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\library\ConstNumericDataType;
use liberty_code_module\handle_model\type\type_integer\library\ConstIntegerDataType;



class IntegerDataType extends NumericDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstIntegerDataType::CONFIG_TYPE,
            ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE => true,
            ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false
        );
    }



}