<?php
/**
 * This class allows to define text data type class.
 * Text data type is string data type,
 * which allows to manage entity attribute multiline string value.
 *
 * Text data type uses the following specified configuration:
 * [
 *     String data type configuration,
 *
 *     type(defined): "text",
 *
 *     multiline_require(defined): true,
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\handle_model\type\type_text\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_string\model\StringDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_string\library\ConstStringDataType;
use liberty_code_module\handle_model\type\type_text\library\ConstTextDataType;



class TextDataType extends StringDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstTextDataType::CONFIG_TYPE,
            ConstStringDataType::TAB_CONFIG_KEY_MULTILINE_REQUIRE => true,
            ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            ConstStringDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false
        );
    }



}