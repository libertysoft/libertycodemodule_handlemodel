<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // Cache services
    // ******************************************************************************

    'attribute_cache_register' => [
        'source' => DefaultTableRegister::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/cache/register/config'],
            ['type' => 'mixed', 'value' => null]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/cache/config'],
            ['type' => 'dependency', 'value' => 'attribute_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Attribute specification cache services
    // ******************************************************************************

    'attribute_specification_cache_register' => [
        'source' => DefaultTableRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'attribute_cache_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'attribute_specification_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'handle_model/attribute/specification/cache/config'],
            ['type' => 'dependency', 'value' => 'attribute_specification_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);