<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // View configuration
    // ******************************************************************************

    'handle_model' => [
        'attribute' => [
            // Cache configuration
            'cache' => [
                'register' => [
                    /**
                     * Configuration array format:
                     * @see DefaultTableRegister configuration format.
                     */
                    'config' => []
                ],

                /**
                 * Configuration format:
                 * @see FormatRepository configuration format.
                 */
                'config' => [
                    'key_pattern' => 'handle-model-attr-%s',
                    'key_regexp_select' => '#handle\-model\-attr\-(.+)#'
                ]
            ],

            'specification' => [
                // Attribute specification cache configuration
                'cache' => [
                    /**
                     * Configuration format:
                     * @see FormatRepository configuration format.
                     */
                    'config' => [
                        'key_pattern' => 'handle-model-attr-spec-%s',
                        'key_regexp_select' => '#handle\-model\-attr\-spec\-(.+)#'
                    ]
                ]
            ]
        ]
    ]
);